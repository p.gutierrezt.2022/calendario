calendar = {}


def add_activity(date: str, time: str, activity: str):
    if date not in calendar:
        calendar[date] = {}
    calendar[date][time] = activity


def get_time(atime):
    """Devuelve todas las actividades para la hora atime como una lista de tuplas"""
    activities = []
    for date, times in calendar.items():
        if atime in times:
            activities.append((date, atime, times[atime]))
    return activities


def get_all():
    """Devuelve todas las actividades en el calendario como una lista de tuplas"""
    all_activities = []
    for date, times in calendar.items():
        for time, activity in times.items():
            all_activities.append((date, time, activity))
    return all_activities

def get_busiest():
    """Devuelve la fecha con más actividades y su número de actividades"""
    cal_num = {}
    lista_comparar = []
    contador = 0
    for fecha in calendar:
        contador = 0
        for hora in calendar[fecha]:
            contador += 1
        cal_num[fecha] = contador

    busiest_no = (max(cal_num.values()))
    busiest = None

    for clave, valor in cal_num.items():
        if valor == busiest_no:
            busiest = clave

    return (busiest, busiest_no)


def show(activities):
    for (date, time, activity) in activities:
        print(f"{date}. {time}: {activity}")


def check_date(date):
    """Comprueba si una fecha tiene el formato correcto (aaaa-mm-dd)
    Devuelve True o False"""

    while True:
        if len(date) != 3:
            return False
        if len(date) == 3:
            año, mes, dia = date
            año = int(año)
            mes = int(mes)
            dia = int(dia)
            if 0 <= año <= 2100 and 1 <= mes <= 12 and 1 <= dia <= 31:
                return True
                break  # Sale del bucle while porque esta en True
            else:
                return False


def check_time(time):
    """Comprueba si una hora tiene el formato correcto (hh:mm)
    Devuelve True o False"""
    # Implementa la lógica de validación de la hora aquí
    while True:
        if len(time) != 2:
            return False
        if len(time) == 2:
            hora, min = time
            hora = int(hora)
            min = int(min)
            if 0 <= hora <= 23 and 0 <= min <= 60:
                return True
                break
            else:
                return False


def get_activity():
    """"Pide al usuario una actividad
    En caso de error al introducirla, vuelve a pedirla desde el principio"""
    while True:
        date = input("Fecha: ")
        parte_date = date.split('-')
        if check_date(parte_date):
            break
        else:
            print("Formato de fecha incorrecto. Inténtalo de nuevo.")

    while True:
        time = input("Hora: ")
        parte_time = time.split(':')
        if check_time(parte_time):
            break
        else:
            print("Formato de hora incorrecto. Inténtalo de nuevo.")

    activity = input("Actividad: ")

    return date, time, activity


def menu():
    print("A. Introduce actividad")
    print("B. Lista todas las actividades")
    print("C. Día más ocupado")
    print("D. Lista de las actividades de cierta hora dada")
    print("X. Terminar")
    asking = True
    while asking:
        option = input("Opción: ").upper()
        if option in ['A', 'B', 'C', 'D', 'X']:
            asking = False
    return option


def run_option(option):
    if option == 'A':
        date, time, activity = get_activity()
        add_activity(date, time, activity)

    elif option == 'B':
        all_activities = get_all()
        show(all_activities)

    elif option == 'C':
        busiest_date, busiest_activities = get_busiest()
        print(f"El día más ocupado es el {busiest_date}, con {busiest_activities} actividad(es).")

    elif option == 'D':
        time = input("Hora: ")
        activities = get_time(time)
        show(activities)


def main():
    proceed = True
    while proceed:
        option = menu()
        if option == 'X':
            proceed = False
        else:
            run_option(option)


if __name__ == "__main__":
    main()
